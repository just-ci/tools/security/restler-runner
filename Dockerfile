ARG PYVERSION=3.9-slim
FROM python:$PYVERSION

COPY . /app

WORKDIR /app

RUN pip install .
