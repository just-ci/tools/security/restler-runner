import json
import logging
import os
from argparse import Namespace
from pathlib import Path
from typing import Any

from junit_xml import TestCase, TestSuite, to_xml_report_file, to_xml_report_string

logger = logging.getLogger(__name__)


def get_bug_bucket(
    mode: Path, status_code: str, request_method: str, request_path: str
) -> str:
    bug_buckets_dir = mode / "RestlerResults" / "bug_buckets"
    bug_buckets = bug_buckets_dir.glob(f"*{status_code}_*.txt")
    for bug_bucket in bug_buckets:
        with open(bug_bucket, "r", encoding="utf-8") as file_stream:
            if f"{request_method} {request_path}" in file_stream.read():
                return str(bug_bucket)
    return ""


def main(args: Namespace) -> None:
    root_dir = args.results
    target_path = args.report

    modes = ["Test", "FuzzLean", "Fuzz"]

    test_cases = []

    for mode in modes:
        if not Path(root_dir / mode).exists():
            logger.warning(f"[-] {mode} folder does not exist. Skipping.")
            continue
        error_buckets_path = root_dir / mode / "ResponseBuckets" / "errorBuckets.json"
        with open(error_buckets_path, "r", encoding="utf-8") as error_buckets_stream:
            error_buckets = json.load(error_buckets_stream)

        for status_code in error_buckets.keys():
            logger.debug(f"Parsing status code {status_code}...")
            for result_id, tests in error_buckets[status_code].items():
                logger.debug(f"Parsing result ID {result_id}...")
                for test in tests:
                    test_cases.append(
                        parse_test(test, status_code, root_dir, mode, args)
                    )

    test_suite = TestSuite("RESTler", test_cases)

    if target_path:
        with open(target_path, "w", encoding="utf-8") as report_stream:
            to_xml_report_file(report_stream, [test_suite], prettyprint=True)
        logger.info(f"[+] Report written to {target_path}.")
    else:
        print(to_xml_report_string([test_suite]))
        logger.info("[+] Report written to stdout.")


def parse_test(
    test: Any, status_code: str, root_dir: Path, mode: str, args: Namespace
) -> TestCase:
    # Build a name from the request and its result
    request_method = test["request"]["RequestData"]["method"]
    request_path = test["request"]["RequestData"]["path"]
    request_query = test["request"]["RequestData"]["query"]
    request_body = (
        test["request"]["RequestData"]["body"].replace("\n", " ").replace("\r", "")
    )

    name_list = []
    if request_path:
        name_list.append(request_path)

    if request_query:
        name_list.append(f"query: {request_query}")

    if request_body:
        name_list.append(f"body: {request_body}")

    name = " - ".join(name_list)

    classname_list = []  # Called Suite in the GitLab interface
    if status_code == "0":  # Not sure what this means in RESTler
        status_code = "?"
    classname_list.append(status_code)
    if request_method:
        classname_list.append(request_method)
    classname = " - ".join(classname_list)

    test_case = TestCase(name, classname=classname)

    if filename := get_bug_bucket(
        root_dir / mode,
        status_code,
        request_method,
        request_path,
    ):
        if os.getenv("CI"):
            branch = os.getenv("CI_COMMIT_REF_NAME")
            if mode == "FuzzLean":
                job = "restler:fuzz-lean"
            else:
                job = f"restler:{mode.lower()}"
            filename = f"{'../'*4}-/jobs/artifacts/{branch}/file/{filename}?job={job}"

        test_case.file = filename

    try:
        response = json.loads(test["response"]["ResponseData"]["content"])
    except json.JSONDecodeError:
        logger.debug("Unable to parse content as JSON. Parsing as raw.")
        response = (
            test["response"]["ResponseData"]["content"]
            .replace("\n", " ")
            .replace("\r", "")
        )
        logger.debug(response)

    if status_code[0] == "5":
        test_case.add_failure_info(response)
    elif status_code[0] == "4" and args.error_on_4xx:
        test_case.add_error_info(response)
    else:
        test_case.add_skipped_info(response)
    logger.debug("Generated test case.")
    return test_case
