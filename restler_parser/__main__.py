import argparse
import logging
import sys
from argparse import Namespace
from importlib.metadata import metadata
from pathlib import Path
from typing import List

import restler_parser
from restler_parser.main import main


def parse_args(argv: List[str] = sys.argv[1:]) -> Namespace:
    package_description = metadata(restler_parser.__package__)["Summary"]
    package_version = metadata(restler_parser.__package__)["Version"]
    description = f"""{package_description} - v{package_version}"""

    parser = argparse.ArgumentParser(
        description=description, prog="python -m restler_parser"
    )
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        "--error-on-4xx",
        help="Parse HTTP 4xx status codes as errors instead of skipped.",
        action="store_true",
    )
    group.add_argument(
        "--errors-only",
        help="Only parse HTTP 500 status codes.",
        action="store_true",
    )
    parser.add_argument(
        "results",
        help="Path to RESTler results (i.e. parent directory of Fuzz, FuzzLean, Test).",
        type=Path,
    )
    parser.add_argument(
        "--report",
        help="Path where to write the JUnit XML report.",
        type=Path,
    )
    parser.add_argument(
        "-v", "--verbose", help="Print debug messages.", action="store_true"
    )

    parsed_args = parser.parse_args(argv)
    return parsed_args


if __name__ == "__main__":
    args = parse_args()
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    main(args)
