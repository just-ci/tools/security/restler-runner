#!/usr/bin/env sh

set -e

cd "$(dirname "$0")"

rm -rf local-testing

mkdir -p local-testing
cd local-testing

echo "[*] Building demo image..."
git clone -b v8.5.0 https://github.com/microsoft/restler-fuzzer.git -c advice.detachedHead=false
echo "jinja2<3.1.0" >> restler-fuzzer/demo_server/requirements.txt
docker build -t restler-demo-server restler-fuzzer/demo_server

echo "[*] Compiling grammar..."
docker run --rm -u "$(id -u):$(id -g)" -v "${PWD}:/pwd" -w /pwd mcr.microsoft.com/restlerfuzzer/restler:v9.0.0 \
    dotnet /RESTler/restler/Restler.dll compile --api_spec restler-fuzzer/demo_server/swagger.json

echo "[*] Running demo server in background for 60 seconds..."
docker kill rester-demo-server || true
timeout --preserve-status --signal SIGINT 60 docker run --rm -p 127.0.0.1:8080:8080 --name rester-demo-server restler-demo-server &

sleep 3  # Wait for server to be ready

echo "[*] Starting fuzzer..."
docker run --rm -u "$(id -u):$(id -g)" -v "${PWD}:/pwd" -w /pwd mcr.microsoft.com/restlerfuzzer/restler:v9.0.0 \
    dotnet /RESTler/restler/Restler.dll test --grammar_file Compile/grammar.py \
    --dictionary_file Compile/dict.json --settings Compile/engine_settings.json \
    --target_ip localhost --target_port 8080 \
    --no_ssl --host localhost:8080

echo "[+] Done. Killing server..."
docker kill rester-demo-server || true

cd "$(dirname "$0")"
