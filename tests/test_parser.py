from restler_parser.__main__ import parse_args
from restler_parser.main import main


def test_parse_to_stdout() -> None:
    main(parse_args(["tests/results"]))


def test_parse_to_file() -> None:
    main(parse_args(["tests/results", "--report", "report.xml"]))


def test_parse_to_file_4xx_as_err() -> None:
    main(parse_args(["tests/results", "--report", "report.xml", "--error-on-4xx"]))


def test_parse_to_file_4xx_as_err_verbose() -> None:
    main(
        parse_args(["tests/results", "--report", "report.xml", "--error-on-4xx", "-v"])
    )


def test_parse_to_file_verbose_errors_only() -> None:
    main(
        parse_args(
            [
                "tests/results",
                "--report",
                "report.xml",
                "-v",
                "--errors-only",
            ]
        )
    )
