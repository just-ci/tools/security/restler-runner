# Setting up a job

Have a `Dockerfile` which builds an image that provides a service, and an
`openapi.yml` containing an OpenAPI spec in YAML format. Customize job if
providing service otherwise, see below.

Add the following to your `.gitlab-ci.yml`:

```yaml
---
include:
  - remote: https://gitlab.com/just-ci/tools/security/restler-runner/-/raw/v1.7.4/templates/restler.yaml
  - remote: https://just-ci.gitlab.io/jobs/v6.7.3/templates/container.yml
```

This will:

- Build the image based on your `Dockerfile`
- Create a RESTler grammar based on your OpenAPI spec
- Start 3 fuzzer jobs:
  - Testing the grammar
  - Doing a 'lean' fuzz, meaning a limited set of tests
  - Doing an actual fuzzing job, which runs for a maximum 1 hour by default
- Produce test results which will be shown in a GitLab merqe request test report

## Customize

You can customize the following options. `$RESTLER_EXTRA_ARGS` can be used for
custom arguments for RESTler. `$PARSER_EXTRA_ARGS` can be used for custom
arguments to the parser.

Override `$RESTLER_EXTRA_ARGS` if your service exposes a port different
than 8080. `--host target:8080` is the default. If you want it set based on your
`openapi.yml` then override this variable to remove the argument.

It is also possible to change how the service is provided to the fuzzer jobs, by
changing the `services:` definition under `.restler:fuzz`. You can for example
set it to `services: null` to start no services if your target is running
somewhere else.

```yaml
variables:
  OPENAPI_SPEC: openapi.yml
  RESTLER_VERSION: v9.0.0
  TARGET_IP: target
  TARGET_PORT: 8080
  RESTLER_GRAMMAR: Compile/grammar.py
  RESTLER_DICTIONARY: Compile/dict.json
  RESTLER_SETTINGS: Compile/engine_settings.json
  RESTLER_EXTRA_ARGS: --no_ssl --host target:8080
  MAX_RUNTIME: 1800 # seconds
  PARSER_DEFAULT_ARGS: --errors-only
  PARSER_EXTRA_ARGS: ""

.restler:fuzz:
  services:
    - name: $IMAGE_NAME:$IMAGE_DEV_TAG
      alias: target
```

### Custom Compile

If you need a custom dictionary compiled into a grammar, create a `config.json`
(per the
[RESTler docs](https://github.com/microsoft/restler-fuzzer/blob/main/docs/user-guide/FuzzingDictionary.md#how-to-configure-the-dictionary))
and point the variable `RESTLER_COMPILE_CONFIG` to it. The paths
(`RESTLER_GRAMMAR` and `RESTLER_DICTIONARY`) set there will be overwritten or
added automatically based on the variables set in your `.gitlab-ci.yml` (or the
defaults) to ensure they correspond to the absolute paths during the CI job.

## Scheduling

To schedule fuzzing when developers are usually away\* you can include the
schedules template which will automatically add GitLab Pipeline Schedules based
on these Lunchtime, Bedtime and Weekend time slots, with appropriate max
durations.

Just add the following to your list of `include:`s:

```yaml
- remote: https://gitlab.com/just-ci/tools/security/restler-runner/-/raw/v1.7.4/templates/schedules.yaml
```

Have a look at [the template](templates/schedules.yaml) to see what the
schedules are, and how they can be modified using variables.

> \*This is based on the results of the paper
> [Effectiveness and Scalability of Fuzzing Techniques in CI/CD Pipelines](https://arxiv.org/abs/2205.14964)

## Development

You can use for example the `demo_server` shipped with RESTler:

1. Clone the `demo_server` code.

```shell
git clone https://github.com/microsoft/restler-fuzzer.git
mv restler-fuzzer/demo_server demo_server
rm -rf restler-fuzzer
```

2. Build and start the container.

```shell
cd demo_server
docker build . -t restler_demo_server
docker network create restler
docker run --rm -ti --network restler --name restler_demo_server restler_demo_server
```

> This attaches to your terminal, so you can see the behavior of your target
> when fuzzing it.

3. Start a RESTler container where the OpenAPI spec is saved.

```shell
cd demo_server
docker run --rm -ti --user "$(id -u):$(id -g)" -v "$PWD":/pwd --workdir /pwd --network restler mcr.microsoft.com/restlerfuzzer/restler:v9.0.0
```

> The following commands are run in that container.

4. Compile the grammar in the RESTler container.

```shell
dotnet /RESTler/restler/Restler.dll compile --api_spec swagger.json
```

5. Run the fuzzer in the RESTler container.

```shell
dotnet /RESTler/restler/Restler.dll fuzz-lean --grammar_file Compile/grammar.py --dictionary_file Compile/dict.json --settings Compile/engine_settings.json --target_ip restler_demo_server --target_port 8888 --no_ssl
```
